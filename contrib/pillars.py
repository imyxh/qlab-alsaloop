#!/usr/bin/env python3
# this is the testing script for pillar work
import matplotlib.pyplot as plt
import numpy as np
import time
import qlab_alsaloop
import sys

feedback = qlab_alsaloop.Feedback()

psize = 256

feedback.set_psize(psize)
feedback.set_gain(0, 0, 0, 1)
#feedback.set_filter(0.15, 0, 0.85, 0.15, 0, 0.85)
feedback.set_filter(1, 0, 0, 1, 0, 0)
feedback.send_parameters()
time.sleep(2)
feedback.set_gain(0, 0, 0, 0)
feedback.send_parameters()
time.sleep(2)

#d = feedback.tune_coarse_phase(feedback.AXIS_YY, 958)
i,o = feedback.tune_coarse_phase(feedback.AXIS_YY, 7136)
print("plus")
feedback.set_delay(i, i, i, i)
feedback.set_gain(0, 0, 0, 1)
feedback.send_parameters()
time.sleep(10)
print("minus")
#feedback.set_delay(o, o, o, o)
#feedback.set_gain(0, 0, 0, 1)
feedback.set_gain(0, 0, 0, -1)
feedback.send_parameters()
time.sleep(20)



























sys.exit(0)




play_periods = 512


phi = (np.arange(play_periods*psize) * 2*np.pi*7100/192000)
src = (2**30 * np.cos(phi)).astype(np.int32)
zer = np.zeros(play_periods*psize).astype(np.int32)

rec = feedback.playrec_frames((src, zer), nperiods=play_periods)
rec_ind = 0

N = psize * 128
plt.plot(src[:N], label="src x")
plt.plot(rec[0][:N], label="rec x")
plt.plot(rec[1][:N], label="rec y")
plt.legend()
plt.show()


discard=128
fomdict = {}
for delay in range(0,200):
    # throw away first `discard` frames, then adjust for delay
    if delay == 0:
        seg_src = np.asarray(src[discard:], dtype="f8")
        seg_rec = np.asarray(rec[rec_ind][discard:], dtype="f8")
    else:
        seg_src = np.asarray(src[discard+delay:], dtype="f8")
        seg_rec = np.asarray(rec[rec_ind][discard:-delay], dtype="f8")
    ip_src = np.dot(seg_src, seg_src)
    ip_rec = np.dot(seg_rec, seg_rec)
    fom = np.dot(seg_src, seg_rec) / np.sqrt(ip_src * ip_rec)
    fomdict[delay] = fom
    print("delay of {} => fom of {}".format(delay, fom))

fomdict = dict(sorted(fomdict.items(), key=lambda x: x[1]))
print("worst delay {} best delay {}".format(
        list(fomdict)[0], list(fomdict)[-1])
)








delay = feedback.tune_coarse_phase(feedback.AXIS_XY, 960.2)

"""
# approx natural freq in cycles per sample:
F0_SAMP = 960.2 / 192000

phi = np.arange(play_periods*psize) * 2*np.pi*F0_SAMP
x = (2**28 * np.cos(phi)).astype(np.int32)
y = np.zeros(play_periods*psize).astype(np.int32)

rec = feedback.playrec_frames((x,y), nperiods=play_periods)

fomdict = {}
for delay in range(0,200):
    # throw away first 256 frames, then adjust for delay
    if delay == 0:
        x_play = np.asarray(x[256:], dtype="f8")
        x_rec = np.asarray(rec[0][256:], dtype="f8")
    else:
        x_play = np.asarray(x[256+delay:], dtype="f8")
        x_rec = np.asarray(rec[0][256:-delay], dtype="f8")
    xxip = np.dot(x_play, x_play)
    XXip = np.dot(x_rec, x_rec)
    fom = np.dot(x_play, x_rec) / np.sqrt(xxip * XXip)
    fomdict[delay] = fom
    print("delay of {} => fom of {}".format(delay, fom))

fomdict = dict(sorted(fomdict.items(), key=lambda x: x[1]))
print("worst {} best {}".format(list(fomdict)[0],list(fomdict)[-1]))
"""

feedback.set_delay(delay, delay, delay, delay)
feedback.set_gain(1, 0, 0, 1)
feedback.send_parameters()
print("positive gain")
time.sleep(5)
feedback.set_gain(-1, 0, 0, -1)
feedback.send_parameters()
print("negative gain")
time.sleep(5)


# old code which did multiple recordings
# (doesn't work bc delay is not recorded)
"""
delays = range(0, 100, 20)
foms = []
recs = []
for delay in delays:

    feedback.set_delay(delay, delay)
    feedback.send_parameters()
    time.sleep(0.2)

    rec = feedback.playrec_frames((x,y), nperiods=play_periods)

    # throw away first 256 frames
    x_play = np.asarray(x[256:], dtype="f8")
    x_rec = np.asarray(rec[0][256:], dtype="f8")

    xxip = np.dot(x_play, x_play)
    XXip = np.dot(x_rec, x_rec)
    print(xxip)
    print(XXip)
    fom = np.dot(x_play, x_rec) / np.sqrt(xxip * XXip)
    foms.append(fom)
    print("delay of {} => fom of {}".format(psize, fom))
    recs.append(rec[0])

del feedback

plt.plot(x[:20*psize])
plt.plot(recs[0][:20*psize], label="0")
plt.plot(recs[1][:20*psize], label="1")
plt.plot(recs[2][:20*psize], label="2")
plt.plot(recs[3][:20*psize], label="3")
plt.plot(recs[4][:20*psize], label="4")
plt.legend()
plt.show()

#plt.plot(foms)
#plt.show()
"""

