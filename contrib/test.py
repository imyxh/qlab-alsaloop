#!/usr/bin/env python3
# this program tests the qlab-alsaloop python wrapper by holding an amplitude
import time
import qlab_alsaloop

feedback = qlab_alsaloop.Feedback()

feedback.set_psize(256)
feedback.set_gain(0, 0.4, -0.4, 0)
feedback.set_filter(1, 0, 0, 1, 0, 0)
feedback.send_parameters()
time.sleep(2)
feedback.tune_coarse_phase()

time.sleep(2)

feedback.start_hold_amp(4, tolerance=0.05, step_xy=0.01, step_yx=-0.01)
time.sleep(20)
feedback.stop_hold_amp()
feedback.start_hold_amp(4, tolerance=0.02, step_xy=0.001, step_yx=-0.001)
time.sleep(10)
feedback.stop_hold_amp()
frames = feedback.get_frames(4096)
time.sleep(2)

