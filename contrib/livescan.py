#!/usr/bin/env python3
# scan frequencies and find phasor response live
import matplotlib.animation as animation
import numpy as np
import matplotlib.pyplot as plt
import pyvisa
import time

rm = pyvisa.ResourceManager()
gpib = rm.open_resource("ASRL/dev/ttyUSB0::INSTR")
active_addr = ""

fs = []
xs = []
ys = []

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)


def animate(i: int):
    # set frequency
    f = 7100 + i
    fs.append(f)
    query_ins("17", "FR"+str(f)+"HZ")
    print(query_ins("17", "IFR"))

    x, y = get_two_phase()
    xs.append(x)
    ys.append(y)

    ax.clear()
    ax.plot(fs, xs, ".", label="Re(V)")
    ax.plot(fs, ys, ".", label="Im(V)")


# instead of separate addr_, get_, and put_ins, we can just use one query_ins
def query_ins(addr: str, cmd: str) -> str:
    # switch to address
    global active_addr
    if addr != active_addr:
        gpib.write("++addr "+addr)
        time.sleep(0.1)
        active_addr = addr
    ret = gpib.query(cmd)
    # to strip of FRHZ\r\n, and also to fix corruption with 7s and 9s
    # (the latter is an issue on the Pi end)
    print(ret)  # DEBUG
    ret = ret.translate({ord("F"): None,
                         ord("R"): None,
                         ord("H"): None,
                         ord("Z"): None,
                         ord("\r"): None,
                         ord("\n"): None})
    return ret


def get_two_phase() -> (float, float):
    xy = query_ins("14", "XY.").split(",")
    try: x = float(xy[0])
    except ValueError:
        print("Error reading X: " + xy[0])
        x = 0
    try: y = float(xy[1])
    except ValueError:
        print("Error reading Y: " + xy[1])
        y = 0
    return (x, y)

if __name__ == "__main__":
    query_ins("17", "AM10MV")
    _ = animation.FuncAnimation(fig, animate, interval=200)
    plt.show()

