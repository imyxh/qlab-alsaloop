#!/usr/bin/env python3
# this program tests the qlab-alsaloop modulation feature
import math
import numpy as np
import time
import qlab_alsaloop
import sys

feedback = qlab_alsaloop.Feedback()

feedback.set_psize(256)
feedback.set_gain(0, 0.4, -0.4, 0)
feedback.set_filter(1, 0, 0, 1, 0, 0)
feedback.send_parameters()
time.sleep(2)
feedback.tune_coarse_phase()

time.sleep(2)

feedback.start_hold_amp(5, tolerance=0.05, step_xy=0.01, step_yx=-0.01)
time.sleep(10)
feedback.stop_hold_amp()
feedback.start_hold_amp(5, tolerance=0.02, step_xy=0.001, step_yx=-0.001)
time.sleep(20)
feedback.stop_hold_amp()

n = 0
data = []

while True:
    feedback.start_hold_amp(5, tolerance=0.02, step_xy=0.001, step_yx=-0.001)
    time.sleep(5)
    feedback.stop_hold_amp()
    tmp = feedback.get_gain()
    feedback.set_gain(0.5, 0, 0, 0)
    feedback.send_parameters()
    time.sleep(0.2)
    feedback.set_recording(500)
    feedback.set_gain(*tmp)
    feedback.send_parameters()
    if n < 500:
        #data.append(feedback.get_recording())
        n += 1
    else:
        print(length(n))
        # TODO: pyplot
        sys.exit()


sys.exit(0)

#f1 = list(map(math.ceil, 2**31*(np.random.rand(1920000)-0.5)))
#f1 = list(map(math.ceil, 10000*[0.95*2**31] + 500000*[0]))

while True:
    feedback.start_hold_amp(5, tolerance=0.02, step_xy=0.001, step_yx=-0.001)
    time.sleep(10)
    feedback.stop_hold_amp()
    tmp = feedback.get_gain()
    feedback.playrec_frames((f1,f1), newfile=False)
    tmp = feedback.set_gain(*tmp)
    feedback.playrec_frames((f1,f1), newfile=False)

