#!/usr/bin/env python3
# this program demonstrates imbalances
import matplotlib.pyplot as plt
import time
import qlab_alsaloop

feedback = qlab_alsaloop.Feedback()

feedback.set_psize(256)
feedback.set_gain(0, 0, 0, 0)
feedback.set_filter(1, 0, 0, 1, 0, 0)
feedback.send_parameters()
time.sleep(2)
feedback.set_gain(0, 1.5, -1.5, 0)
frames = feedback.get_frames(4096)[100:1000]
plt.plot(*frames)
plt.show()
print("recording done! exit whenever.")

time.sleep(60*5)

