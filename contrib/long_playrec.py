#!/usr/bin/env python3
# this is for testing performance
import matplotlib.pyplot as plt
import numpy as np
import time
import qlab_alsaloop
import sys

# approx natural freq in cycles per sample:
F0_SAMP = 960.2 / 192000

feedback = qlab_alsaloop.Feedback()

psize = 256

feedback.set_psize(psize)
feedback.set_gain(1, 0, 0, 0)
feedback.set_filter(0.15, 0, 0.85, 0.15, 0, 0.85)
feedback.send_parameters()
time.sleep(3)
feedback.set_gain(0, 0, 0, 0)
feedback.send_parameters()
time.sleep(2)

play_periods = 2**10

phi = np.arange(play_periods*psize) * 2*np.pi*F0_SAMP
x = (2**30 * np.cos(phi)).astype(np.int32)
y = np.zeros(play_periods*psize).astype(np.int32)
recs = []

rec = feedback.playrec_frames((x,y), nperiods=play_periods)

del feedback

plt.plot(x)
plt.plot(rec[0])
plt.show()

