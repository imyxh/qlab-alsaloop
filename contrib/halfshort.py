#!/usr/bin/env python3
# testing phase differences with y channel on oscillator and x shorted to itself
import matplotlib.pyplot as plt
import numpy as np
import time
import qlab_alsaloop
import sys

# drive slightly off resonance (cycles per sample)
F0_SAMP = 7000 / 192000

feedback = qlab_alsaloop.Feedback()

psize = 256

feedback.set_psize(psize)
feedback.set_gain(1, 0, 0, 0)
feedback.set_filter(0.15, 0, 0.85, 0.15, 0, 0.85)
feedback.send_parameters()
time.sleep(3)
feedback.set_gain(0, 0, 0, 0)
feedback.send_parameters()
time.sleep(2)

play_periods = 512

phi = np.arange(play_periods*psize) * 2*np.pi*F0_SAMP
x = (2**24 * np.cos(phi)).astype(np.int32)
y = (2**31 * np.cos(phi)).astype(np.int32)

x, y = map(lambda a: np.array(a,dtype="<i4"),
    feedback.playrec_frames((x,y), nperiods=play_periods)
)

#x, y = map(lambda a: np.array(a,dtype="f8"), feedback.get_frames(4))

#line = 4E9/192 * np.arange(len(x))
#resid = x - line

del feedback

#plt.plot(resid, "o-")
plt.plot(x, "o-")
plt.plot(y, "o-")
plt.show()

