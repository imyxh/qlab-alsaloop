#!/usr/bin/env python3
# scan frequencies and find phasor response, writing a csv
import csv
import numpy as np
import pyvisa
import time

rm = pyvisa.ResourceManager()
gpib = rm.open_resource("ASRL/dev/ttyUSB0::INSTR")
active_addr = ""

fs = []
res = []
ims = []


# instead of separate addr_, get_, and put_ins, we can just use one query_ins
def query_ins(addr: str, cmd: str) -> str:
    # switch to address
    global active_addr
    if addr != active_addr:
        gpib.write("++addr "+addr)
        time.sleep(0.1)
        active_addr = addr
    ret = gpib.query(cmd)
    # to strip of FRHZ\r\n, and also to fix corruption with 7s and 9s
    ret = ret.translate({ord("F"): None,
                         ord("R"): None,
                         ord("H"): None,
                         ord("Z"): None,
                         ord("\r"): None,
                         ord("\n"): None,
                        })
    return ret


def get_two_phase() -> (float, float):
    xy = query_ins("14", "XY.").split(",")
    try:
        x = float(xy[0])
    except ValueError:
        print("Error reading X: " + xy[0])
        x = 0
    try:
        y = float(xy[1])
    except ValueError:
        print("Error reading Y: " + xy[1])
        y = 0
    return (x, y)


if __name__ == "__main__":
    query_ins("17", "AM10MV")
    for d in range(0, 300):
        f = 7000 + d
        fs.append(f)
        # add sleep here if you want
        query_ins("17", "FR{}HZ".format(f))
        (re, im) = get_two_phase()
        res.append(re)
        ims.append(im)
        # DEBUG
        print(f, re, im)
    # csv export
    with open("scanfreq.csv", "w") as csvfile:
        writer = csv.writer(csvfile)
        for i, f in enumerate(fs):
            writer.writerow([f, res[i], ims[i]])
    print("data written to scanfreq.csv in the format f, Re, Im")

