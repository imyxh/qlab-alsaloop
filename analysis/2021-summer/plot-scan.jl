#!/usr/bin/julia
using CSV
using Plots; gr()

x = CSV.File(open("scanx.csv"); header=["f", "re", "im"])
y = CSV.File(open("scany.csv"); header=["f", "re", "im"])

default(fontfamily="computer modern")
scalefontsizes(4)
p1 = plot(x.f, [x.re, x.im],
          title="re and im of scan x",
          xlabel="f (Hz)", ylabel="re or im voltage (V)",
          size=(2048,2048))
println("graph plotted")
savefig("scanx.png")
println("saved to scanx.png")

p2 = plot(y.f, [y.re, y.im],
          title="re and im of scan y",
          xlabel="f (Hz)", ylabel="re or im voltage (V)",
          size=(2048,2048))
println("graph plotted")
savefig("scany.png")
println("saved to scany.png")

#TODO: have not tested this. julia is slow as hell on the pi.

