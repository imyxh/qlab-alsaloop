#!/usr/bin/julia
using Plots
gr()

struct Frame
    ch1::Int32
    ch2::Int32
end
function Base.read(io::IO, ::Type{Frame})
    Frame(read(io,Int32),read(io,Int32))
end

mV = 1.389373796494296E-8   # mV per integer step
fs = 192000

x = Float32[]
y = Float32[]
open("ding1.bin", "r") do io
    while !eof(io)
        push!(x, read(io, Frame).ch1 * mV)
        push!(y, read(io, Frame).ch2 * mV)
    end
end
println("data imported")

p = plot(zeros(0), zeros(0), xlims = (-5, 5), ylims = (-5, 5))
anim = Animation()
#for i in 1:length(x)
for i in 1:10
    push!(p, x[i], y[i])
    frame(anim)
end

gif(anim, "xy.gif", fps=60)

