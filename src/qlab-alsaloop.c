#include <sys/mman.h>
#include <unistd.h>
#include <sched.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <pthread.h>
#include "qlab-alsaloop.h"

// ignore the return-local-addr warning from open_stream()
#pragma GCC diagnostic ignored "-Wreturn-local-addr"

// keep in mind that setting DEBUG will drop the performance!
#define DEBUG
//#define VDEBUG
//#define VVDEBUG
// set this if you want to use a test triangular waveform instead of feedback:
//#define TESTWAV
// note that we do not use err.h pretty-printing, to maximize performance.
// fprintf will do.

// autostop on xrun?
#define XRUNAUTOSTOP

// TODO: switch loop params to pointer, so it can be atomically switched out?
// or otherwise copy loop params at beginning of every loop so we avoid race
// condition bs
loop_params_t loop_params;
rec_status_t rec_status = { .bufcount = 0, .state = done };
rec_status_t play_status = { .bufcount = 0, .state = done };

snd_pcm_t *capt_handle;
snd_pcm_t *play_handle;
snd_pcm_hw_params_t *capt_params;
snd_pcm_hw_params_t *play_params;

frame_t *norec_buf_in;	// holds new frames before filtering, if not recording
frame_t *buf_out;	// is written to sound card at end

pthread_t ipc_thread;
int rc;		// return code variable
int dir;	// alsa writes to this, we don't use it

unsigned int fs = SAMPLE_RATE;


static inline void rec_cleanup()
{
	for (int p = 0; p < rec_status.bufcount; p++) {
		free(rec_status.bufs[p]);
		rec_status.bufs[p] = 0;
	}
}


static inline void play_cleanup()
{
	for (int p = 0; p < play_status.bufcount; p++) {
		free(play_status.bufs[p]);
		play_status.bufs[p] = 0;
	}
}


void abort_feedback()
{
	pthread_kill(ipc_thread, SIGINT);
	snd_pcm_drain(capt_handle);
	snd_pcm_close(capt_handle);
	snd_pcm_drain(play_handle);
	snd_pcm_close(play_handle);
	rec_cleanup();
	play_cleanup();
	rec_status.state = exit_thread;
	pthread_join(ipc_thread, 0);
	free(norec_buf_in);
	norec_buf_in = 0;
	free(buf_out);
	buf_out = 0;
	exit(EXIT_SUCCESS);
}


int set_psize(snd_pcm_t *handle, snd_pcm_hw_params_t *params, int dir)
{
	snd_pcm_uframes_t actual_psize;
	snd_pcm_hw_params_set_period_size_near(handle, params,
		(snd_pcm_uframes_t *)&loop_params.psize, &dir);
	snd_pcm_hw_params_get_period_size(params, &actual_psize, &dir);
	if (actual_psize != loop_params.psize) {
		fprintf(stderr, "tried psize %d, got %d\n", loop_params.psize,
			(uint32_t)actual_psize);
		return 1;
	}
	return 0;
}


// open a stream to "default" and return a pointer to its paramaters struct
snd_pcm_hw_params_t *open_stream(snd_pcm_t **handle, int dir)
{

	snd_pcm_hw_params_t *params;

	// open PCM device
	rc = snd_pcm_open(handle, "hw:2,0", dir, 0);
	if (rc < 0) {
		fprintf(stderr, "unable to open pcm device: %s\n",
			snd_strerror(rc));
		exit(EXIT_FAILURE);
	}

	// allocate hardware parameters object and fill with default values
	snd_pcm_hw_params_alloca(&params);
	snd_pcm_hw_params_any(*handle, params);

	// interleaved mode
	snd_pcm_hw_params_set_access(*handle, params,
		SND_PCM_ACCESS_RW_INTERLEAVED);
	// signed 32-bit little-endian integer format (floats are terrible)
	snd_pcm_hw_params_set_format(*handle, params, SND_PCM_FORMAT_S32_LE);
	// 2 channels
	snd_pcm_hw_params_set_channels(*handle, params, 2);
	// set sample rate
	snd_pcm_hw_params_set_rate_near(*handle, params, &fs, &dir);
	if (fs != SAMPLE_RATE) {
		fprintf(stderr, "couldn't set sample rate to %d; got %d\n",
			SAMPLE_RATE, fs);
		exit(EXIT_FAILURE);
	}
	// set frames per period exactly
	if (set_psize(*handle, params, dir)) {
		exit(EXIT_FAILURE);
	}

	// write the parameters to the driver
	rc = snd_pcm_hw_params(*handle, params);
	if (rc < 0) {
		fprintf(stderr, "unable to set hw parameters: %s\n",
			snd_strerror(rc));
		exit(EXIT_FAILURE);
	}

#ifndef XRUNAUTOSTOP
	// sw params (to disable xrun autostop)
	snd_pcm_sw_params_t *swparams;
	snd_pcm_sw_params_alloca(&swparams);
	snd_pcm_sw_params_current(*handle, swparams);
	snd_pcm_uframes_t boundary;
	snd_pcm_sw_params_get_boundary(swparams, &boundary);
	snd_pcm_sw_params_set_stop_threshold(*handle, swparams, boundary);
	rc = snd_pcm_sw_params(*handle, swparams);
	if (rc < 0) {
		fprintf(stderr, "unable to set sw parameters: %s\n",
			snd_strerror(rc));
		exit(EXIT_FAILURE);
	};
#endif

	return params;

}


// parent process to handle ipc
void *ipc_worker()
{
	int rc;

	uint32_t prev_psize = loop_params.psize;

	// read loop_params struct from stdin
	while (rec_status.state != exit_thread) {
#ifdef DEBUG
		// TODO: I have a lot of these behind DEBUG guards, but I
		// shouldn't need them, as this is its own thread, right?
		puts("waiting for struct");
#endif
		while (!read(0, &loop_params, sizeof(loop_params))) {
			// we need to nop here because we want to wait even if
			// read() returns 0; that way we can handle SIGINT still
		}
		if (loop_params.psize != prev_psize) {
			if (loop_params.psize > MAX_PSIZE) {
				fprintf(stderr, "psize of %d is too large!\n",
					loop_params.psize);
				abort_feedback(SIGINT);
			}
			/* we cannot change ALSA's psize nicely! :(
			if (set_psize(capt_handle, capt_params,
				SND_PCM_STREAM_CAPTURE) ||
				set_psize(play_handle, play_params,
				SND_PCM_STREAM_PLAYBACK))
			{
				fprintf(stderr, "failed to change psize\n");
				abort_feedback(SIGINT);
			}
			*/
		}

#ifdef DEBUG
		printf("new parameters set: Gxx=%f, Gxy=%f, Gyx=%f, Gyy=%f"
			"\nax=%f, bx=%f, rax=%f, ay=%f, by=%f, ray=%f\n"
			"new period size is %d frames "
			"with delays (%d,%d,%d,%d)\n",
			loop_params.gain_matrix.xx,
			loop_params.gain_matrix.xy,
			loop_params.gain_matrix.yx,
			loop_params.gain_matrix.yy,
			loop_params.filter_coeffs.ax,
			loop_params.filter_coeffs.bx,
			loop_params.filter_coeffs.rax,
			loop_params.filter_coeffs.ay,
			loop_params.filter_coeffs.by,
			loop_params.filter_coeffs.ray,
			loop_params.psize,
			(int)loop_params.delay_xx,
			(int)loop_params.delay_xy,
			(int)loop_params.delay_yx,
			(int)loop_params.delay_yy
		);
#endif

		// setup play and rec
		if (loop_params.play_periods) {
			play_cleanup();
			// allocate individual buffers
			for (int p = 0; p < loop_params.play_periods; p++) {
				play_status.bufs[p] = (frame_t *)malloc(
					loop_params.psize * BYTES_PER_FRAME
				);
			}
#ifdef DEBUG
			printf("modulation set for %d periods\n",
				loop_params.play_periods
			);
#endif
			// read data from file
			FILE *data = fopen("modulation.bin", "r");
			for (int p = 0; p < loop_params.play_periods; p++) {
				rc = fread(play_status.bufs[p],
					BYTES_PER_FRAME, loop_params.psize, data
				);
			}
			fclose(data);
			play_status.bufcount = loop_params.play_periods;
		}
		if (loop_params.rec_periods) {
			rec_cleanup();
			// allocate individual buffers
			for (int p = 0; p < loop_params.rec_periods; p++) {
				rec_status.bufs[p] = (frame_t *)malloc(
					loop_params.psize * BYTES_PER_FRAME
				);
			}
#ifdef DEBUG
			printf("recording set for %d periods\n",
				loop_params.rec_periods
			);
#endif
			rec_status.bufcount = loop_params.rec_periods;
		}

		// trigger play and rec
		if (loop_params.play_periods)
			play_status.state = todo;
		if (loop_params.rec_periods)
			rec_status.state = todo;

		// cleanup play and rec
		if (loop_params.play_periods) {
			while (play_status.state != done) {
				// wait for modulation to finish
				usleep(1000);
			}
#ifdef DEBUG
			puts("modulation finished");
#endif
		}
		if (loop_params.rec_periods) {
			while (rec_status.state != done) {
				// wait for recording to finish
				usleep(1000);
			}
			puts("recording finished");
			// write data to file
			FILE *data = fopen("recording.bin", "w");
			for (int p = 0; p < loop_params.rec_periods; p++) {
				rc = fwrite(rec_status.bufs[p],
					BYTES_PER_FRAME, loop_params.psize, data
				);
#ifdef VDEBUG
				printf("%d out of %d frames written for p %d\n",
						rc, loop_params.psize, p);
#endif
			}
			fclose(data);
			// write a "done" file so python knows we are done
			// writing the data
			data = fopen("recording.bin.done", "w");
			fclose(data);
		}

	}

	puts("exiting from ipc_worker thread");
	pthread_exit(EXIT_SUCCESS);
}


int main()
{

	signal(SIGINT, abort_feedback);

	// initialize loop parameters with some default values
	loop_params = (loop_params_t) {
		.psize = 256,
		.rec_periods = 0,
		.play_periods = 0,
		.gain_matrix = {
			.xx = 0,	.xy = 1,
			.yx = 1,	.yy = 0,
		},
		.filter_coeffs = {
			.ax = 1, .bx = 0, .rax = 0,
			.ay = 1, .by = 0, .ray = 0,
		},
		.delay_xx = 0, .delay_xy = 0,
		.delay_yx = 0, .delay_yy = 0,
	};

	capt_params = open_stream(&capt_handle, SND_PCM_STREAM_CAPTURE);
	play_params = open_stream(&play_handle, SND_PCM_STREAM_PLAYBACK);
	if (!capt_params | !play_params) {
		fprintf(stderr, "failed to open capture/playback stream\n");
		exit(EXIT_FAILURE);
	}
	puts("hw parameters have been set");

	// old frames (post linear filter)
	frame_t old_frames[SAVED_FRAMES];
	// last frame of previous period, pre-filter
	frame_t last_frame_pre = {0};
	// last frame of previous period, post-filter (but pre-gain)
	frame_t last_frame_post = {0};

	// buf_in if recording is not set
	norec_buf_in = (frame_t *) malloc(MAX_PSIZE * BYTES_PER_FRAME);

	// our precious output buffer
	buf_out = (frame_t *) malloc(MAX_PSIZE * BYTES_PER_FRAME);

	// create ipc thread
	rc = pthread_create(&ipc_thread, 0, ipc_worker, 0);
	play_status.state = done;
	rec_status.state = done;

	// set scheduling priority
	struct sched_param sched_param;
	if (sched_getparam(0, &sched_param) < 0) {
		puts("scheduler getparam failed");
		abort_feedback(SIGINT);
	}
	sched_param.sched_priority = SCHED_PRIORITY;
	if (sched_setscheduler(0, SCHED_RR, &sched_param)) {
		printf("setscheduler for round robin with priority %d failed\n",
			sched_param.sched_priority);
		abort_feedback(SIGINT);
	}
	printf("scheduler set to round robin with priority %d\n",
		sched_param.sched_priority);

	while (1) {

		// default to no recording
		frame_t *buf_in = norec_buf_in;
		if (play_status.state == todo) {
			play_status.state = doing;
			play_status.index = 0;
		}
		if (rec_status.state == todo) {
			rec_status.state = doing;
			rec_status.index = 0;
		}
		if (rec_status.state == doing
		&& rec_status.index < rec_status.bufcount) {
#ifdef VDEBUG
			printf("recording period %d\n", rec_status.index);
#endif
			buf_in = rec_status.bufs[rec_status.index];
		}

		// use constant psize and delays per loop
		uint32_t cur_psize = loop_params.psize;
		uint8_t cur_delay_xx = loop_params.delay_xx;
		uint8_t cur_delay_xy = loop_params.delay_xy;
		uint8_t cur_delay_yx = loop_params.delay_yx;
		uint8_t cur_delay_yy = loop_params.delay_yy;
		// effective saved frames
		uint32_t saved_frames;
		if (cur_psize < SAVED_FRAMES) {
			saved_frames = cur_psize;
		} else {
			saved_frames = SAVED_FRAMES;
		}

		rc = snd_pcm_readi(capt_handle, buf_in, cur_psize);

// TODO: panic on xrun
#ifdef XRUNAUTOSTOP
		if (rc == -EPIPE) {
			snd_pcm_prepare(capt_handle);
#ifdef DEBUG
			fprintf(stderr, ANSI_COLOR_RED
				"!! OVERRUN OCCURRED !!\n" ANSI_COLOR_RESET
			);
			//abort_feedback(SIGINT);
		} else if (rc < 0) {
			fprintf(stderr, "error from readi: %s\n",
				snd_strerror(rc));
		} else if (rc < (int)cur_psize) {
			fprintf(stderr, "short read, read %d frames\n", rc);
#endif
		}
#endif

#ifdef TESTWAV
		for (int f = 0; f < cur_psize; f++) {
			(buf_in + f)->ch1 = f % 32;
			(buf_in + f)->ch2 = f % 32;
		}
#endif

		frame_t tmp_last_frame_pre = buf_in[cur_psize-1];
		// start from the end of buf_in and work backwards, applying
		// linear filtering for NONRECURSIVE COEFFICIENTS. we stop short
		// of the first frame.
		for (int f = cur_psize - 1; f > 0; f--) {
			buf_in[f].ch1 *= loop_params.filter_coeffs.ax;
			buf_in[f].ch1 += buf_in[f-1].ch1
				* loop_params.filter_coeffs.bx;
		}
		for (int f = cur_psize - 1; f > 0; f--) {
			buf_in[f].ch2 *= loop_params.filter_coeffs.ay;
			buf_in[f].ch2 += buf_in[f-1].ch2
				* loop_params.filter_coeffs.by;
		}
		// handle the first frame, where the previous frame was saved
		buf_in[0].ch1 *= loop_params.filter_coeffs.ax;
		buf_in[0].ch1 += last_frame_pre.ch1
			* loop_params.filter_coeffs.bx;
		buf_in[0].ch2 *= loop_params.filter_coeffs.ay;
		buf_in[0].ch2 += last_frame_pre.ch2
			* loop_params.filter_coeffs.by;
		// save last frame for next loop
		last_frame_pre = tmp_last_frame_pre;

		// work forwards for RECURSIVE COEFFICIENTS
		// handle the first frame again
		buf_in[0].ch1 += last_frame_post.ch1
			* loop_params.filter_coeffs.rax;
		buf_in[0].ch2 += last_frame_post.ch2
			* loop_params.filter_coeffs.ray;
		// edge case is now dealt with, so we can continue simply.
		for (uint32_t f = 1; f < cur_psize; f++) {
			buf_in[f].ch1 += buf_in[f-1].ch1
				* loop_params.filter_coeffs.rax;
			buf_in[f].ch2 += buf_in[f-1].ch2
				* loop_params.filter_coeffs.ray;
		}
		last_frame_post = buf_in[cur_psize-1];

		// if more coefficients are added, deal with them in the loops
		// above :(

		// iterate through each frame in the period, forwards again, to
		// multiply the frame vector by the gain matrix, after adjusting
		// for delays. we want to do this one element of the matrix at a
		// time, so that we can properly index into saved frames
		// quickly. note that this new approach loses some resolution,
		// since we round to int32 one extra time :(
		// XX GAIN:
		for (uint32_t f = 0; f < cur_delay_xx; f++) {
			buf_out[f].ch1 = (int32_t)(
				loop_params.gain_matrix.xx * old_frames[
					SAVED_FRAMES - cur_delay_xx + f
				].ch1
			);
		}
		for (uint32_t f = cur_delay_xx; f < cur_psize; f++) {
			buf_out[f].ch1 = (int32_t)(
				loop_params.gain_matrix.xx
					* buf_in[f-cur_delay_xx].ch1
			);
		}
		// XY GAIN:
		for (uint32_t f = 0; f < cur_delay_xy; f++) {
			buf_out[f].ch1 += (int32_t)(
				loop_params.gain_matrix.xy * old_frames[
					SAVED_FRAMES - cur_delay_xy + f
				].ch2
			);
		}
		for (uint32_t f = cur_delay_xy; f < cur_psize; f++) {
			buf_out[f].ch1 += (int32_t)(
				loop_params.gain_matrix.xy
					* buf_in[f-cur_delay_xy].ch2
			);
		}
		// YX GAIN:
		for (uint32_t f = 0; f < cur_delay_yx; f++) {
			buf_out[f].ch2 = (int32_t)(
				loop_params.gain_matrix.yx * old_frames[
					SAVED_FRAMES - cur_delay_yx + f
				].ch1
			);
		}
		for (uint32_t f = cur_delay_yx; f < cur_psize; f++) {
			buf_out[f].ch2 = (int32_t)(
				loop_params.gain_matrix.yx
					* buf_in[f-cur_delay_yx].ch1
			);
		}
		// YY GAIN:
		for (uint32_t f = 0; f < cur_delay_yy; f++) {
			buf_out[f].ch2 += (int32_t)(
				loop_params.gain_matrix.yy * old_frames[
					SAVED_FRAMES - cur_delay_yy + f
				].ch2
			);
		}
		for (uint32_t f = cur_delay_yy; f < cur_psize; f++) {
			buf_out[f].ch2 += (int32_t)(
				loop_params.gain_matrix.yy
					* buf_in[f-cur_delay_yy].ch2
			);
		}

		// TODO: this might hang for large psizes??
		// copy new old frames (apparently memcpy causes bugs?)
		// go backwards, putting them at the end of old_frames
		for (uint32_t f = 1; f <= saved_frames; f++) {
			old_frames[SAVED_FRAMES-f] = buf_in[cur_psize - f];
		}


		if (play_status.state == doing) {
			frame_t *play_buf = play_status.bufs[play_status.index];
			for (uint32_t f = 0; f < cur_psize; f++) {
				frame_t *out_f = buf_out + f;
				out_f->ch1 += play_buf[f].ch1;
				out_f->ch2 += play_buf[f].ch2;
			}
		}

#ifdef VVDEBUG
		// test a frame in the middle of our period
#define TEST_FRAME 64
		printf("(%d, %d) -> (%d, %d)\n",
			(buf_in+TEST_FRAME)->ch1, (buf_in+TEST_FRAME)->ch2,
			(buf_out+TEST_FRAME)->ch1, (buf_out+TEST_FRAME)->ch2);
#endif

		rc = snd_pcm_writei(play_handle, buf_out, cur_psize);

// TODO: panic on xrun
#ifdef XRUNAUTOSTOP
		if (rc == -EPIPE) {
			snd_pcm_prepare(play_handle);
#ifdef DEBUG
			fprintf(stderr, ANSI_COLOR_RED
				"!! UNDERRUN OCCURRED !!\n" ANSI_COLOR_RESET
			);
			//abort_feedback(SIGINT);
		} else if (rc < 0) {
			fprintf(stderr, "error from writei: %s\n",
				snd_strerror(rc));
		} else if (rc < (int)cur_psize) {
			fprintf(stderr, "short write, wrote %d frames\n", rc);
#endif
		}
#endif

		if (play_status.state == doing) {
			play_status.index++;
			if (play_status.index >= play_status.bufcount) {
				play_status.state = done;
			}
		}
		if (rec_status.state == doing) {
			rec_status.index++;
			if (rec_status.index >= rec_status.bufcount) {
				rec_status.state = done;
			}
		}

	}

	return 1;

}

