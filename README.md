qlab-alsaloop
=============

A project at Wesleyan University for the Exotic Waves lab, allowing for low-cost
and reproducible means of controlling and studying an oscillator.


Why not existing software?
--------------------------

We have tried using a combination of ALSA softvol modules and JACK to provide
feedback. This works fine, except for two main drawbacks:

 1. the resolution of phase shift as set by `jack_bufsize` is limited by the
    sample rate, and
 2. it is difficult to implement separate xx, xy, yx, and yy gain parameters
    without introducing additional complexity and latency.

Moreover, this low-level implementation of a feedback loop allows us to
efficiently use linear filtering techniques to set phase shifts and band-pass
filters.


Targeted setup
--------------

The targeted system is a Raspberry Pi 4B, with Hifiberry DAC+ ADC Pro, running
Raspbian with the PREEMPT-RT kernel patch. We are working with a 7 kHz aluminum
bar oscillator controlled by four piezoelectric transducers such that it can
oscillate with feedback in two dimensions.


Raspberry Pi system preparation
-------------------------------

We have some important changes on our Raspberry Pi:

/boot/cmdline.txt is edited to include an `isolcpus=3` parameter, so that the
fourth CPU core (numbered 3) is isolated for our purposes.

/boot/config.txt is set to the following (don't copy this exactly, just for
reference):

```
# For more options and information see
# http://rpf.io/configtxt
# Some settings may impact device functionality. See link above for details

# uncomment if you get no picture on HDMI for a default "safe" mode
#hdmi_safe=1

# uncomment this if your display has a black border of unused pixels visible
# and your display can output without overscan
disable_overscan=1

# uncomment the following to adjust overscan. Use positive numbers if console
# goes off screen, and negative if there is too much border
#overscan_left=16
#overscan_right=16
#overscan_top=16
#overscan_bottom=16

# uncomment to force a console size. By default it will be display's size minus
# overscan.
#framebuffer_width=1280
#framebuffer_height=720

# uncomment if hdmi display is not detected and composite is being output
#hdmi_force_hotplug=1

# uncomment to force a specific HDMI mode (this will force VGA)
#hdmi_group=1
#hdmi_mode=1

# uncomment to force a HDMI mode rather than DVI. This can make audio work in
# DMT (computer monitor) modes
#hdmi_drive=2

# uncomment to increase signal to HDMI, if you have interference, blanking, or
# no display
#config_hdmi_boost=4

# uncomment for composite PAL
#sdtv_mode=2

#uncomment to overclock the arm. 700 MHz is the default.
#arm_freq=800

# Uncomment some or all of these to enable the optional hardware interfaces
#dtparam=i2c_arm=on
#dtparam=i2s=on
#dtparam=spi=on

# Uncomment this to enable infrared communication.
#dtoverlay=gpio-ir,gpio_pin=17
#dtoverlay=gpio-ir-tx,gpio_pin=18

# hifiberry (added by ihuang@wesleyan.edu)
dtoverlay=hifiberry-dacplusadcpro
dtoverlay=i2s-mmap

# Additional overlays and parameters are documented /boot/overlays/README

# Enable audio (loads snd_bcm2835)
dtparam=audio=on

[pi4]
# Enable DRM VC4 V3D driver on top of the dispmanx display stack
dtoverlay=vc4-fkms-v3d
max_framebuffers=2

[all]
#dtoverlay=vc4-fkms-v3d
[all]
kernel=vmlinuz-5.10.35-rt39-v7l+
# initramfs initrd.img-5.10.35-rt39-v7l+
os_prefix=5.10.35-rt39-v7l+/
overlay_prefix=overlays/
[all]
```

/etc/asound.conf is given the following configuration to ensure that the
"default" sound device is our Hifiberry sound card:

```
pcm.!default {
  type hw
  card 2
  rate 192000
}
ctl.!default {
  type hw
  card 2
  rate 192000
}
```

We have further used raspi-config to set the pi to autologin to the console,
rather than spinning up a desktop environment. This is to save resources to
maximize performance.

Perhaps the most important change is that we are using kdoren's [5.10.35-rt39
Linux kernel](https://github.com/kdoren/linux/releases/tag/5.10.35-rt39), with
the PREEMPT-RT patch set. This allows for more reliable scheduling for our code.
In turn, we need to give ourselves permissions for this realtime scheduling. In
/etc/security/limits.conf, add:

```
@audio           -       rtprio          95
@audio           -       memlock         512000
@audio           -       nice            -19
```

In the future, it would be nice to upgrade the kernel occasionally. But due to
this custom kernel patch, be wary of blindly running `apt upgrade`! We only want
to update the Linux kernel as the patch is updated in accord.

For VISA instrumentation like our oscilloscope, we need this line in
`/etc/udev/rules.d/99-com.rules` in order to give users privileges to use
pyvisa:

```
SUBSYSTEM=="usb", MODE="0666", GROUP="usbusers"
```

Lastly, there is a setup.sh script in this source code, which does the simple
job of making sure that all of the scaling governors for each cpu core are set
to "performance," rather than "ondemand." Once again, this is just for a
performance boost, to make sure we are running at high clock speeds.


Building
--------

It should be simple enough to compile the all code in this repository by simply
typing `make`.


Python API
----------

I have written two Python scripts that are meant to be used as libraries:
  - `qlab_visa.py` should handle very simple communication over the GPIB bus, to
    the scope, and to other scientific instruments;
  - `qlab_alsaloop.py` handles the interfacing with my C feedback loop routines.


Example files
-------------

I have included many example files and will probably add more as time goes on,
to show what this library can do:
  - `scanfreq.py` uses the GPIB bus and some instruments in our lab to do a
    frequency sweep to find the real and imaginary parts of phasor response;
  - `piscandat.py` does the same but using the `sounddevice` Python library to
    play and record right from the Pi (currently, this is feature-incomplete);
  - `livescan.py` also does the same but live, using a pyplot animation;
  - `xyresponse.py` is an example of how to use the main library of this
    repository, `qlab-alsaloop`, to record the x and y oscillations of a system
    hooked up to the sound card.

As more files get added to this repository, I will probably consider
restructuring to create separate `lib` and `examples` directories.


How it works
------------

This software follows something similar to a client-server model. The main
program, `qlab-alsaloop`, is the "server," and will roughly do the following:

 1. Initialize the audio input streams;
 2. create a worker thread to handle communication with the client;
 3. allocate memory for input and output buffers;
 4. read interleaved samples ("frames"), adjust them via the linear recursive
    filter, and apply the gain matrix;
 5. write the adjusted frames to be outputted by the sound card;
 6. repeat steps 4 and 5 until given the signal SIGINT;
 7. close audio streams and clean up.

Meanwhile, the worker thread will:

 1. Wait for new parameters to be sent into its stdin;
 2. apply the new parameters via shared memory with the main thread;
 3. if recording was turned on, allocate buffers for recording;
 4. write recording to a file `recording.bin`;
 5. repeat from step 1.

NOTE: it has been a while since recording has been tested, and code has changed
since. There may be bugs. Feel free to reach out to me at <ihuang@wesleyan.edu>.

For experimental purposes, it is likely that you, the physicist, will want to
avoid touching the low-level C code. No worries. There is a Python API provided,
`qlab_alsaloop.py`. An example of a very simple program one might write with
this API would be:

```python
#!/usr/bin/env python3
import time
import qlab_alsaloop

feedback = qlab_alsaloop.Feedback()

feedback.set_psize(256)
feedback.set_gain(0, 0.4, -0.4, 0)  # set the gain matrix
feedback.set_filter(1, 0, 0, 1, 0, 0)   # set filtering parameters
feedback.send_parameters()
time.sleep(2)
feedback.tune_coarse_phase()    # tune both channels of phase

time.sleep(2)

# hold an amplitude with start_hold_amp
feedback.start_hold_amp(4, tolerance=0.05, step_xy=0.01, step_yx=-0.01)
time.sleep(20)
feedback.stop_hold_amp()
# do it again with finer tolerance
feedback.start_hold_amp(4, tolerance=0.02, step_xy=0.001, step_yx=-0.001)
time.sleep(10)
feedback.stop_hold_amp()
# get data with get_frames
frames = feedback.get_frames(4096)
time.sleep(2)
```

It is a good idea to put these scripts into the `contrib` directory (if put in
the repo at all), and ensure that the root of this repository is in
`$PYTHONPATH`.


TODO
----

- better XRUN infrastructure (toggle XRUN aborts from a flag variable in
  parameter struct?)
- allow setting recursive params in qmixer
- add rbx, rby if needed
- pipe qmixer output to a fifo file somewhere instead of polluting the tui

