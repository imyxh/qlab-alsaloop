#!/usr/bin/env python3
import pyvisa
import time


# this should be treated as a singleton class!
class InstrumentControl:
    active_addr = ""

    def __init__(self):
        self.rm = pyvisa.ResourceManager()
        self.gpib = self.rm.open_resource("ASRL/dev/ttyUSB0::INSTR")

    # instead of separate addr_, get_, and put_ins, we can just use one query_ins
    def query_ins(self, addr: str, cmd: str) -> str:
        # switch to address
        if addr != InstrumentControl.active_addr:
            self.gpib.write("++addr "+addr)
            time.sleep(0.1)
            InstrumentControl.active_addr = addr
        ret = self.gpib.query(cmd)
        # to strip of FRHZ\r\n, and also to fix corruption with 7s and 9s
        # (the latter is an issue on the Pi end)
        print(ret)  # DEBUG
        ret = ret.translate({ord("F"): None,
                             ord("R"): None,
                             ord("H"): None,
                             ord("Z"): None,
                             ord("\r"): None,
                             ord("\n"): None,
                             ord("/"): ord("+"),
                             ord("?"): ord("7"),
                             ord("="): ord("9")})
        return ret

    def get_two_phase(self) -> (float, float):
        xy = self.query_ins("14", "XY.").split(",")
        try:
            x = float(xy[0])
        except ValueError:
            print("Error reading X: " + xy[0])
            x = 0
        try:
            y = float(xy[1])
        except ValueError:
            print("Error reading Y: " + xy[1])
            y = 0
        return (x, y)

